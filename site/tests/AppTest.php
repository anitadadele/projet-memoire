<?php

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;

class AppTest extends TestCase
{
    public function testRootUrlReturns200()
    {
        $client = new Client(['base_uri' => 'http://localhost:8080']); // Adjust base URI as needed
        $response = $client->get('/');

        $this->assertEquals(200, $response->getStatusCode());
    }
}
